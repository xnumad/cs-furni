using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Flazzy.ABC.AVM2.Instructions;
using Sulakore;
using Sulakore.Communication;
using Sulakore.Modules;
using Sulakore.Protocol;
using Tangine;

namespace CS_Furni
{
    [Module("CS-Furni", "Place lots of furniture clientside")]
    public partial class GUI : ExtensionForm
    {
        bool directionUp = false;
        Item myItem = new Item();
        public GUI()
        {
            InitializeComponent();
        }

        private void GUI_Load(object sender, EventArgs e)
        {
            Text = "CS-Furni v" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            nmr_interval_ValueChanged(null, null);
            propertyGrid_item.SelectedObject = myItem;
            LoadFurnidata();
        }

        private void LoadFurnidata()
        {
            var xmlDocument = new XmlDocument();

            //Download
            var furnidata = new WebClient()
            {
                Headers = {
                        {
                            "User-Agent",
                            "Mozilla/5.0"
                        }
                    },
                Encoding = Encoding.UTF8 //furnis with name containing "Piñata" //should be the default anyways -_-
            }.DownloadString(Hotel.ToUrl(true) + "/gamedata/furnidata_xml/1");

            //Parse method 1 (using System.Xml.Linq), easier than the other one
            var doc = XDocument.Parse(furnidata);
            var roomFurnis = doc.Descendants("furnitype").Where(b => b.Parent.Name == "roomitemtypes"); //filter for roomitemtypes (floor furni) because wallitemtypes aren't supported at all in this module

            foreach (XElement furnitureNode in roomFurnis)
            {
                var id = furnitureNode.Attribute("id").Value;
                var classname = furnitureNode.Attribute("classname").Value;
                var name = furnitureNode.Element("name").Value;
                var description = furnitureNode.Element("description").Value;
                var furniline = furnitureNode.Element("furniline").Value;
                var revision = furnitureNode.Element("revision").Value;
                var defaultdir = furnitureNode.Element("defaultdir").Value;
                var xdim = furnitureNode.Element("xdim").Value;
                var ydim = furnitureNode.Element("ydim").Value;
                var offerid = furnitureNode.Element("offerid").Value;
                var buyout = furnitureNode.Element("buyout").Value;
                var bc = furnitureNode.Element("bc").Value;
                dataGridView_furnis.Rows.Add(id, classname, name, description, furniline, revision, defaultdir, xdim, ydim, offerid, buyout, bc); //Add to table
            }

            //Parse method 2 //Derived from Mika's Furni Editor
            //xmlDocument.LoadXml(furnidata);
            //
            //XmlNodeList furniElements = xmlDocument.GetElementsByTagName("furnitype");
            //
            //foreach (XmlNode furnitureNode in furniElements)
            //{
            //    var id = furnitureNode.Attributes["id"].Value;
            //    var classname = furnitureNode.Attributes["classname"].InnerText;
            //
            //    foreach (XmlNode propertyNode in furnitureNode)
            //    {
            //        if (propertyNode.Name == "name")
            //        {
            //            var name = propertyNode.InnerText;
            //            dataGridView_furnis.Rows.Add(id, classname, name); //Add to table
            //        }
            //    }
            //}

            dataGridView_furnis.Rows[dataGridView_furnis.CurrentCell.RowIndex].Selected = true; //Select whole row instead of just first cell
        }

        [OutDataCapture("RoomUserWalk")]
        private void CatchWalkPacket(DataInterceptedEventArgs obj)
        {
            if (chk_updateCoordsOnWalk.Checked)
            {
                HMessage RoomUserWalk = obj.Packet;
                myItem.xcoord = RoomUserWalk.ReadInteger();
                myItem.ycoord = RoomUserWalk.ReadInteger();
                propertyGrid_item.Refresh();
            }
            if (chk_placeOnWalk.Checked)
                PlaceFurni();
            if (chk_blockWalk.Checked)
                obj.IsBlocked = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                PlaceFurni();
                DataGridViewSelectNextRow(dataGridView_furnis); //Go to the next furni (because we want it to be shown the next time) by selecting the next row (automatically triggers dataGridView_furnis_CurrentCellChanged)
                UpdateCoordinatesForNextFurniture();
            }
            catch (ArgumentOutOfRangeException) //end of table reached
            {
                chk_active.Checked = false;
            }
        }

        private void DataGridViewSelectNextRow(DataGridView dataGridView, int howManyRowsToJump = 1)
        {
            dataGridView.CurrentCell = dataGridView[dataGridView.CurrentCell.ColumnIndex, (dataGridView.CurrentCell.RowIndex + howManyRowsToJump)]; //https://docs.microsoft.com/dotnet/framework/winforms/controls/get-and-set-the-current-cell-wf-datagridview-control#to-set-the-current-cell-programmatically
            dataGridView_furnis.Rows[(dataGridView.CurrentCell.RowIndex)].Selected = true; //Rather highlight whole row
        }

        private void chk_active_CheckedChanged(object sender, EventArgs e)
        {
            timer.Enabled = chk_active.Checked;
        }

        private void UpdateCoordinatesForNextFurniture()
        {
            if (!directionUp && myItem.xcoord <= nmr_maximumxcoord.Value && myItem.ycoord <= nmr_maximumycoord.Value) //going down, both coords are within limit
            {
                myItem.xcoord += Convert.ToInt32(nmr_furnidistance_x.Value);
                myItem.ycoord += Convert.ToInt32(nmr_furnidistance_y.Value);
            }
            else if (directionUp && myItem.xcoord >= nmr_minimumxcoord.Value && myItem.ycoord >= nmr_minimumycoord.Value) //going up, both coords are within limit
            {
                myItem.xcoord -= Convert.ToInt32(nmr_furnidistance_x.Value);
                myItem.ycoord -= Convert.ToInt32(nmr_furnidistance_y.Value);
            }
            else //at least one coord has reached its direction limit
            {
                //Next column
                myItem.xcoord += Convert.ToInt32(nmr_changeby_x.Value);
                myItem.ycoord += Convert.ToInt32(nmr_changeby_y.Value);

                //Because we want a frontal square, not an isometric one
                nmr_maximumxcoord.Value += Math.Abs(nmr_changeby_x.Value);
                nmr_maximumycoord.Value += Math.Abs(nmr_changeby_y.Value);
                nmr_minimumxcoord.Value -= Math.Abs(nmr_changeby_x.Value);
                nmr_minimumycoord.Value -= Math.Abs(nmr_changeby_y.Value);

                directionUp = !directionUp; //change direction
            }
        }

        private async void PlaceFurni()
        {
            await Connection.SendToClientAsync(
                    new HMessage(
                        In.AddFloorItem,
                        myItem.itemId,
                        myItem.furniId,
                        myItem.xcoord,
                        myItem.ycoord,
                        myItem.direction,
                        myItem.zcoord,
                        myItem.sizeZ, //furni height, seems to be ignored anyways
                        myItem.extraDataVariable,
                        myItem.extraDataType,
                        myItem.state, //extrDataString for extraDataType = 0
                        myItem.rentTime,
                        myItem.usagePolicy,
                        myItem.ownerId,
                        myItem.ownerName
                        ).ToBytes()
                        );
            myItem.itemId++; //increase itemid because there can only be one item of its id
            propertyGrid_item.Refresh();
        }

        private void btn_place_Click(object sender, EventArgs e)
        {
            PlaceFurni();
        }

        private void chk_TopMost_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = chk_TopMost.Checked;
        }

        private void dataGridView_furnis_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                myItem.direction = Convert.ToInt32(dataGridView_furnis.CurrentRow.Cells["defaultdir"].Value);
                myItem.furniId = Convert.ToInt32(dataGridView_furnis.CurrentRow.Cells["id"].Value);
                //Last changed value will be selected in the PropertyGrid
                propertyGrid_item.Refresh();
                UpdateFurniPreview();
            }
            catch (NullReferenceException) //no row selected, e.g. after sorting a column
            {
            }
        }

        private void UpdateFurniPreview()
        {
            if (tabControl1.SelectedTab == tabPage_Preview) //Only update preview if the tab is actually selected to prevent unnecessary slowdowns (especially on mass placing)
            {
                webBrowser_furni_preview.DocumentText = Properties.Resources.furni_preview_stripped.Replace("undefined", "\"" + dataGridView_furnis.CurrentRow.Cells["classname"].Value.ToString() + "\"");
            }
        }

        private void propertyGrid_item_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem.PropertyDescriptor.Name == nameof(myItem.furniId)) //Compare names to see if furniId is the changed property //That comparison took really long to find //Found at https://github.com/vchelaru/FlatRedBall/blob/0028430/FRBDK/Glue/Glue/Controls/FileBuildToolAssociationWindow.cs#L108 via https://csharp.hotexamples.com/de/examples/System.Windows.Forms/PropertyValueChangedEventArgs/-/php-propertyvaluechangedeventargs-class-examples.html#0x784705a3645065082a1e3d94c6b13c8266e5096bb4469460a429f3f1cb3eca3e-28,,52,
            {
                DataGridViewSelectFirstRowWhereCellValueInColumnEquals(dataGridView_furnis, "id", myItem.furniId); //Select the row for the just set furniId in the table to show details
            }
        }

        private bool DataGridViewSelectFirstRowWhereCellValueInColumnEquals(DataGridView dataGridView, string columnName, int valueToBeFound)
        {
            DataGridViewRow rowToBeFound = null; //The sought-after row

            //Find row with either or one of the following code blocks, both are working

            //https://stackoverflow.com/a/18802556
            try
            {
                rowToBeFound = dataGridView.Rows
                    .OfType<DataGridViewRow>()
                     .Where(x => Convert.ToInt32(x.Cells[columnName].Value) == valueToBeFound)
                     .ToArray<DataGridViewRow>()[0];
            }
            catch //Throws exception if value was not found, rowToBeFound remains being null but that NullReferenceException is being catched at the end of this method
            {
            }

            //https://stackoverflow.com/a/6265333
            //foreach (DataGridViewRow row in dataGridView.Rows)
            //{
            //    if (Convert.ToInt32(row.Cells[columnName].Value) == valueToBeFound)
            //    {
            //        rowToBeFound = row;
            //        break; //only select the first row and because it was just found here, stop further searching for another row
            //    }
            //}

            try
            {
                dataGridView.CurrentCell = rowToBeFound.Cells[columnName]; //Triggers both CurrentCellChanged and SelectionChanged events
                rowToBeFound.Selected = true; //Triggers only SelectionChanged event
                return true;
            }
            catch (NullReferenceException) //rowToBeFound == null because it wasn't found
            {
                dataGridView.ClearSelection(); //Don't select anything and clear the current selection //The row header arrow still remains
                return false;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFurniPreview();
        }

        private void linkLabel_Source_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/xnumad/cs-furni");
        }

        private void nmr_interval_ValueChanged(object sender, EventArgs e)
        {
            timer.Interval = (int)nmr_interval.Value;
        }
    }

    class Item
    {
        [Description("An itemId can only be used once per room to be able to unambiguously identify items")]
        public int itemId { get; set; } = 1;
        public int furniId { get; set; } = 13;

        [Category("Placement")]
        public int xcoord { get; set; } = 1;
        [Category("Placement")]
        public int ycoord { get; set; } = 1;
        [Category("Placement")]
        public int direction { get; set; } = 0;
        [Category("Placement")]
        public string zcoord { get; set; } = "0.0";
        [Category("Placement")]
        [Description("Stack height")]
        public string sizeZ { get; set; } = "1.0";
        [Category("Extradata")]
        public int extraDataVariable { get; set; } = 0;
        [Category("Extradata")]
        public int extraDataType { get; set; } = 0;
        [Category("Extradata")]
        public string state { get; set; } = "0";
        public int rentTime { get; set; } = -1;
        public int usagePolicy { get; set; } = 0;
        [Category("Owner")]
        public int ownerId { get; set; } = 1;
        [Category("Owner")]
        public string ownerName { get; set; } = "Clientside";
    }
}