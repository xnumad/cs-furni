using System.Reflection;

namespace CS_Furni
{
    partial class GUI
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lbl_interval = new System.Windows.Forms.Label();
            this.nmr_interval = new System.Windows.Forms.NumericUpDown();
            this.chk_active = new System.Windows.Forms.CheckBox();
            this.lbl_maximum_coords = new System.Windows.Forms.Label();
            this.nmr_maximumxcoord = new System.Windows.Forms.NumericUpDown();
            this.nmr_maximumycoord = new System.Windows.Forms.NumericUpDown();
            this.lbl_furnidistance = new System.Windows.Forms.Label();
            this.nmr_furnidistance_x = new System.Windows.Forms.NumericUpDown();
            this.nmr_furnidistance_y = new System.Windows.Forms.NumericUpDown();
            this.lbl_changeby = new System.Windows.Forms.Label();
            this.nmr_changeby_x = new System.Windows.Forms.NumericUpDown();
            this.nmr_changeby_y = new System.Windows.Forms.NumericUpDown();
            this.grp_mass = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nmr_minimumycoord = new System.Windows.Forms.NumericUpDown();
            this.lbl_minimum_coords = new System.Windows.Forms.Label();
            this.nmr_minimumxcoord = new System.Windows.Forms.NumericUpDown();
            this.grp_walk = new System.Windows.Forms.GroupBox();
            this.chk_blockWalk = new System.Windows.Forms.CheckBox();
            this.chk_updateCoordsOnWalk = new System.Windows.Forms.CheckBox();
            this.chk_placeOnWalk = new System.Windows.Forms.CheckBox();
            this.btn_place = new System.Windows.Forms.Button();
            this.propertyGrid_item = new System.Windows.Forms.PropertyGrid();
            this.dataGridView_furnis = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.furniline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.revision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.defaultdir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xdim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ydim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.offerid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buyout = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chk_TopMost = new System.Windows.Forms.CheckBox();
            this.grp_form = new System.Windows.Forms.GroupBox();
            this.webBrowser_furni_preview = new System.Windows.Forms.WebBrowser();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_Placement = new System.Windows.Forms.TabPage();
            this.tabPage_Preview = new System.Windows.Forms.TabPage();
            this.linkLabel_Source = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_interval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_maximumxcoord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_maximumycoord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_furnidistance_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_furnidistance_y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_changeby_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_changeby_y)).BeginInit();
            this.grp_mass.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_minimumycoord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_minimumxcoord)).BeginInit();
            this.grp_walk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_furnis)).BeginInit();
            this.grp_form.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage_Placement.SuspendLayout();
            this.tabPage_Preview.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lbl_interval
            // 
            this.lbl_interval.AutoSize = true;
            this.lbl_interval.Location = new System.Drawing.Point(3, 173);
            this.lbl_interval.Name = "lbl_interval";
            this.lbl_interval.Size = new System.Drawing.Size(63, 13);
            this.lbl_interval.TabIndex = 0;
            this.lbl_interval.Text = "interval [ms]";
            // 
            // nmr_interval
            // 
            this.nmr_interval.Location = new System.Drawing.Point(72, 171);
            this.nmr_interval.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_interval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmr_interval.Name = "nmr_interval";
            this.nmr_interval.Size = new System.Drawing.Size(60, 20);
            this.nmr_interval.TabIndex = 1;
            this.nmr_interval.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nmr_interval.ValueChanged += new System.EventHandler(this.nmr_interval_ValueChanged);
            // 
            // chk_active
            // 
            this.chk_active.AutoSize = true;
            this.chk_active.Location = new System.Drawing.Point(6, 197);
            this.chk_active.Name = "chk_active";
            this.chk_active.Size = new System.Drawing.Size(56, 17);
            this.chk_active.TabIndex = 3;
            this.chk_active.Text = "Active";
            this.chk_active.UseVisualStyleBackColor = true;
            this.chk_active.CheckedChanged += new System.EventHandler(this.chk_active_CheckedChanged);
            // 
            // lbl_maximum_coords
            // 
            this.lbl_maximum_coords.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_maximum_coords.AutoSize = true;
            this.lbl_maximum_coords.Location = new System.Drawing.Point(8, 86);
            this.lbl_maximum_coords.Name = "lbl_maximum_coords";
            this.lbl_maximum_coords.Size = new System.Drawing.Size(85, 13);
            this.lbl_maximum_coords.TabIndex = 0;
            this.lbl_maximum_coords.Text = "maximum coords";
            // 
            // nmr_maximumxcoord
            // 
            this.nmr_maximumxcoord.Location = new System.Drawing.Point(99, 79);
            this.nmr_maximumxcoord.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_maximumxcoord.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_maximumxcoord.Name = "nmr_maximumxcoord";
            this.nmr_maximumxcoord.Size = new System.Drawing.Size(50, 20);
            this.nmr_maximumxcoord.TabIndex = 1;
            this.nmr_maximumxcoord.Value = new decimal(new int[] {
            48,
            0,
            0,
            0});
            // 
            // nmr_maximumycoord
            // 
            this.nmr_maximumycoord.Location = new System.Drawing.Point(155, 79);
            this.nmr_maximumycoord.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_maximumycoord.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_maximumycoord.Name = "nmr_maximumycoord";
            this.nmr_maximumycoord.Size = new System.Drawing.Size(55, 20);
            this.nmr_maximumycoord.TabIndex = 1;
            this.nmr_maximumycoord.Value = new decimal(new int[] {
            48,
            0,
            0,
            0});
            // 
            // lbl_furnidistance
            // 
            this.lbl_furnidistance.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_furnidistance.AutoSize = true;
            this.lbl_furnidistance.Location = new System.Drawing.Point(23, 12);
            this.lbl_furnidistance.Name = "lbl_furnidistance";
            this.lbl_furnidistance.Size = new System.Drawing.Size(70, 13);
            this.lbl_furnidistance.TabIndex = 0;
            this.lbl_furnidistance.Text = "furni distance";
            // 
            // nmr_furnidistance_x
            // 
            this.nmr_furnidistance_x.Location = new System.Drawing.Point(99, 3);
            this.nmr_furnidistance_x.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_furnidistance_x.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_furnidistance_x.Name = "nmr_furnidistance_x";
            this.nmr_furnidistance_x.Size = new System.Drawing.Size(50, 20);
            this.nmr_furnidistance_x.TabIndex = 1;
            this.nmr_furnidistance_x.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // nmr_furnidistance_y
            // 
            this.nmr_furnidistance_y.Location = new System.Drawing.Point(155, 3);
            this.nmr_furnidistance_y.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_furnidistance_y.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_furnidistance_y.Name = "nmr_furnidistance_y";
            this.nmr_furnidistance_y.Size = new System.Drawing.Size(55, 20);
            this.nmr_furnidistance_y.TabIndex = 1;
            this.nmr_furnidistance_y.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lbl_changeby
            // 
            this.lbl_changeby.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_changeby.AutoSize = true;
            this.lbl_changeby.Location = new System.Drawing.Point(12, 114);
            this.lbl_changeby.Name = "lbl_changeby";
            this.lbl_changeby.Size = new System.Drawing.Size(81, 26);
            this.lbl_changeby.TabIndex = 0;
            this.lbl_changeby.Text = "on limit change coords by";
            // 
            // nmr_changeby_x
            // 
            this.nmr_changeby_x.Location = new System.Drawing.Point(99, 112);
            this.nmr_changeby_x.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_changeby_x.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_changeby_x.Name = "nmr_changeby_x";
            this.nmr_changeby_x.Size = new System.Drawing.Size(50, 20);
            this.nmr_changeby_x.TabIndex = 1;
            this.nmr_changeby_x.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // nmr_changeby_y
            // 
            this.nmr_changeby_y.Location = new System.Drawing.Point(155, 112);
            this.nmr_changeby_y.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_changeby_y.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_changeby_y.Name = "nmr_changeby_y";
            this.nmr_changeby_y.Size = new System.Drawing.Size(55, 20);
            this.nmr_changeby_y.TabIndex = 1;
            this.nmr_changeby_y.Value = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            // 
            // grp_mass
            // 
            this.grp_mass.Controls.Add(this.tableLayoutPanel1);
            this.grp_mass.Controls.Add(this.chk_active);
            this.grp_mass.Controls.Add(this.nmr_interval);
            this.grp_mass.Controls.Add(this.lbl_interval);
            this.grp_mass.Location = new System.Drawing.Point(302, 6);
            this.grp_mass.Name = "grp_mass";
            this.grp_mass.Size = new System.Drawing.Size(227, 220);
            this.grp_mass.TabIndex = 6;
            this.grp_mass.TabStop = false;
            this.grp_mass.Text = "Mass Place";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.19444F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.80556F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel1.Controls.Add(this.nmr_maximumycoord, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.nmr_minimumycoord, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.nmr_changeby_x, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.nmr_changeby_y, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_furnidistance, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.nmr_maximumxcoord, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_minimum_coords, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_maximum_coords, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.nmr_minimumxcoord, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.nmr_furnidistance_y, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.nmr_furnidistance_x, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_changeby, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(214, 146);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // nmr_minimumycoord
            // 
            this.nmr_minimumycoord.Location = new System.Drawing.Point(155, 41);
            this.nmr_minimumycoord.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_minimumycoord.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_minimumycoord.Name = "nmr_minimumycoord";
            this.nmr_minimumycoord.Size = new System.Drawing.Size(55, 20);
            this.nmr_minimumycoord.TabIndex = 1;
            // 
            // lbl_minimum_coords
            // 
            this.lbl_minimum_coords.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_minimum_coords.AutoSize = true;
            this.lbl_minimum_coords.Location = new System.Drawing.Point(11, 50);
            this.lbl_minimum_coords.Name = "lbl_minimum_coords";
            this.lbl_minimum_coords.Size = new System.Drawing.Size(82, 13);
            this.lbl_minimum_coords.TabIndex = 0;
            this.lbl_minimum_coords.Text = "minimum coords";
            // 
            // nmr_minimumxcoord
            // 
            this.nmr_minimumxcoord.Location = new System.Drawing.Point(99, 41);
            this.nmr_minimumxcoord.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmr_minimumxcoord.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.nmr_minimumxcoord.Name = "nmr_minimumxcoord";
            this.nmr_minimumxcoord.Size = new System.Drawing.Size(50, 20);
            this.nmr_minimumxcoord.TabIndex = 1;
            // 
            // grp_walk
            // 
            this.grp_walk.Controls.Add(this.chk_blockWalk);
            this.grp_walk.Controls.Add(this.chk_updateCoordsOnWalk);
            this.grp_walk.Controls.Add(this.chk_placeOnWalk);
            this.grp_walk.Location = new System.Drawing.Point(302, 232);
            this.grp_walk.Name = "grp_walk";
            this.grp_walk.Size = new System.Drawing.Size(227, 96);
            this.grp_walk.TabIndex = 7;
            this.grp_walk.TabStop = false;
            this.grp_walk.Text = "Walk";
            // 
            // chk_blockWalk
            // 
            this.chk_blockWalk.AutoSize = true;
            this.chk_blockWalk.Checked = true;
            this.chk_blockWalk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_blockWalk.Location = new System.Drawing.Point(7, 67);
            this.chk_blockWalk.Name = "chk_blockWalk";
            this.chk_blockWalk.Size = new System.Drawing.Size(78, 17);
            this.chk_blockWalk.TabIndex = 2;
            this.chk_blockWalk.Text = "Block walk";
            // 
            // chk_updateCoordsOnWalk
            // 
            this.chk_updateCoordsOnWalk.AutoSize = true;
            this.chk_updateCoordsOnWalk.Checked = true;
            this.chk_updateCoordsOnWalk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_updateCoordsOnWalk.Location = new System.Drawing.Point(7, 44);
            this.chk_updateCoordsOnWalk.Name = "chk_updateCoordsOnWalk";
            this.chk_updateCoordsOnWalk.Size = new System.Drawing.Size(136, 17);
            this.chk_updateCoordsOnWalk.TabIndex = 1;
            this.chk_updateCoordsOnWalk.Text = "Update coords on walk";
            this.chk_updateCoordsOnWalk.UseVisualStyleBackColor = true;
            // 
            // chk_placeOnWalk
            // 
            this.chk_placeOnWalk.AutoSize = true;
            this.chk_placeOnWalk.Checked = true;
            this.chk_placeOnWalk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_placeOnWalk.Location = new System.Drawing.Point(7, 20);
            this.chk_placeOnWalk.Name = "chk_placeOnWalk";
            this.chk_placeOnWalk.Size = new System.Drawing.Size(93, 17);
            this.chk_placeOnWalk.TabIndex = 0;
            this.chk_placeOnWalk.Text = "Place on walk";
            this.chk_placeOnWalk.UseVisualStyleBackColor = true;
            // 
            // btn_place
            // 
            this.btn_place.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_place.Location = new System.Drawing.Point(0, 398);
            this.btn_place.Name = "btn_place";
            this.btn_place.Size = new System.Drawing.Size(296, 23);
            this.btn_place.TabIndex = 3;
            this.btn_place.Text = "Place";
            this.btn_place.UseVisualStyleBackColor = true;
            this.btn_place.Click += new System.EventHandler(this.btn_place_Click);
            // 
            // propertyGrid_item
            // 
            this.propertyGrid_item.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.propertyGrid_item.Location = new System.Drawing.Point(0, 2);
            this.propertyGrid_item.Name = "propertyGrid_item";
            this.propertyGrid_item.Size = new System.Drawing.Size(296, 397);
            this.propertyGrid_item.TabIndex = 8;
            this.propertyGrid_item.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid_item_PropertyValueChanged);
            // 
            // dataGridView_furnis
            // 
            this.dataGridView_furnis.AllowUserToOrderColumns = true;
            this.dataGridView_furnis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_furnis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_furnis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.classname,
            this.name,
            this.description,
            this.furniline,
            this.revision,
            this.defaultdir,
            this.xdim,
            this.ydim,
            this.offerid,
            this.buyout,
            this.bc});
            this.dataGridView_furnis.Location = new System.Drawing.Point(549, 0);
            this.dataGridView_furnis.Name = "dataGridView_furnis";
            this.dataGridView_furnis.Size = new System.Drawing.Size(455, 447);
            this.dataGridView_furnis.TabIndex = 9;
            this.dataGridView_furnis.CurrentCellChanged += new System.EventHandler(this.dataGridView_furnis_CurrentCellChanged);
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Width = 40;
            // 
            // classname
            // 
            this.classname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.classname.HeaderText = "classname";
            this.classname.Name = "classname";
            this.classname.Width = 82;
            // 
            // name
            // 
            this.name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.name.HeaderText = "name";
            this.name.Name = "name";
            this.name.Width = 58;
            // 
            // description
            // 
            this.description.HeaderText = "description";
            this.description.Name = "description";
            // 
            // furniline
            // 
            this.furniline.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.furniline.HeaderText = "furniline";
            this.furniline.Name = "furniline";
            this.furniline.Width = 68;
            // 
            // revision
            // 
            this.revision.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.revision.HeaderText = "revision";
            this.revision.Name = "revision";
            this.revision.Width = 68;
            // 
            // defaultdir
            // 
            this.defaultdir.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.defaultdir.HeaderText = "defaultdir";
            this.defaultdir.Name = "defaultdir";
            this.defaultdir.Width = 21;
            // 
            // xdim
            // 
            this.xdim.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.xdim.HeaderText = "xdim";
            this.xdim.Name = "xdim";
            this.xdim.Width = 21;
            // 
            // ydim
            // 
            this.ydim.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ydim.HeaderText = "ydim";
            this.ydim.Name = "ydim";
            this.ydim.Width = 21;
            // 
            // offerid
            // 
            this.offerid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.offerid.HeaderText = "offerid";
            this.offerid.Name = "offerid";
            this.offerid.Width = 21;
            // 
            // buyout
            // 
            this.buyout.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.buyout.HeaderText = "buyout";
            this.buyout.Name = "buyout";
            this.buyout.Width = 21;
            // 
            // bc
            // 
            this.bc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.bc.HeaderText = "bc";
            this.bc.Name = "bc";
            this.bc.Width = 21;
            // 
            // chk_TopMost
            // 
            this.chk_TopMost.AutoSize = true;
            this.chk_TopMost.Checked = true;
            this.chk_TopMost.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_TopMost.Location = new System.Drawing.Point(8, 19);
            this.chk_TopMost.Name = "chk_TopMost";
            this.chk_TopMost.Size = new System.Drawing.Size(68, 17);
            this.chk_TopMost.TabIndex = 10;
            this.chk_TopMost.Text = "TopMost";
            this.chk_TopMost.UseVisualStyleBackColor = true;
            this.chk_TopMost.CheckedChanged += new System.EventHandler(this.chk_TopMost_CheckedChanged);
            // 
            // grp_form
            // 
            this.grp_form.Controls.Add(this.chk_TopMost);
            this.grp_form.Location = new System.Drawing.Point(302, 334);
            this.grp_form.Name = "grp_form";
            this.grp_form.Size = new System.Drawing.Size(227, 48);
            this.grp_form.TabIndex = 7;
            this.grp_form.TabStop = false;
            this.grp_form.Text = "Form";
            // 
            // webBrowser_furni_preview
            // 
            this.webBrowser_furni_preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser_furni_preview.Location = new System.Drawing.Point(0, 0);
            this.webBrowser_furni_preview.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser_furni_preview.Name = "webBrowser_furni_preview";
            this.webBrowser_furni_preview.Size = new System.Drawing.Size(535, 421);
            this.webBrowser_furni_preview.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_Placement);
            this.tabControl1.Controls.Add(this.tabPage_Preview);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(543, 447);
            this.tabControl1.TabIndex = 11;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage_Placement
            // 
            this.tabPage_Placement.Controls.Add(this.linkLabel_Source);
            this.tabPage_Placement.Controls.Add(this.propertyGrid_item);
            this.tabPage_Placement.Controls.Add(this.grp_mass);
            this.tabPage_Placement.Controls.Add(this.grp_walk);
            this.tabPage_Placement.Controls.Add(this.btn_place);
            this.tabPage_Placement.Controls.Add(this.grp_form);
            this.tabPage_Placement.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Placement.Name = "tabPage_Placement";
            this.tabPage_Placement.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Placement.Size = new System.Drawing.Size(535, 421);
            this.tabPage_Placement.TabIndex = 0;
            this.tabPage_Placement.Text = "Placement";
            this.tabPage_Placement.UseVisualStyleBackColor = true;
            // 
            // tabPage_Preview
            // 
            this.tabPage_Preview.Controls.Add(this.webBrowser_furni_preview);
            this.tabPage_Preview.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Preview.Name = "tabPage_Preview";
            this.tabPage_Preview.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Preview.Size = new System.Drawing.Size(535, 421);
            this.tabPage_Preview.TabIndex = 1;
            this.tabPage_Preview.Text = "Preview";
            this.tabPage_Preview.UseVisualStyleBackColor = true;
            // 
            // linkLabel_Source
            // 
            this.linkLabel_Source.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel_Source.AutoSize = true;
            this.linkLabel_Source.Location = new System.Drawing.Point(488, 403);
            this.linkLabel_Source.Name = "linkLabel_Source";
            this.linkLabel_Source.Size = new System.Drawing.Size(41, 13);
            this.linkLabel_Source.TabIndex = 9;
            this.linkLabel_Source.TabStop = true;
            this.linkLabel_Source.Text = "Source";
            this.linkLabel_Source.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_Source_LinkClicked);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 447);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dataGridView_furnis);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "GUI";
            this.Text = "CS-Furni";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.GUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nmr_interval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_maximumxcoord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_maximumycoord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_furnidistance_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_furnidistance_y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_changeby_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_changeby_y)).EndInit();
            this.grp_mass.ResumeLayout(false);
            this.grp_mass.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_minimumycoord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmr_minimumxcoord)).EndInit();
            this.grp_walk.ResumeLayout(false);
            this.grp_walk.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_furnis)).EndInit();
            this.grp_form.ResumeLayout(false);
            this.grp_form.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage_Placement.ResumeLayout(false);
            this.tabPage_Placement.PerformLayout();
            this.tabPage_Preview.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lbl_interval;
        private System.Windows.Forms.NumericUpDown nmr_interval;
        private System.Windows.Forms.CheckBox chk_active;
        private System.Windows.Forms.Label lbl_maximum_coords;
        private System.Windows.Forms.NumericUpDown nmr_maximumxcoord;
        private System.Windows.Forms.NumericUpDown nmr_maximumycoord;
        private System.Windows.Forms.Label lbl_furnidistance;
        private System.Windows.Forms.NumericUpDown nmr_furnidistance_x;
        private System.Windows.Forms.NumericUpDown nmr_furnidistance_y;
        private System.Windows.Forms.Label lbl_changeby;
        private System.Windows.Forms.NumericUpDown nmr_changeby_x;
        private System.Windows.Forms.NumericUpDown nmr_changeby_y;
        private System.Windows.Forms.GroupBox grp_mass;
        private System.Windows.Forms.GroupBox grp_walk;
        private System.Windows.Forms.Button btn_place;
        private System.Windows.Forms.CheckBox chk_blockWalk;
        private System.Windows.Forms.CheckBox chk_updateCoordsOnWalk;
        private System.Windows.Forms.CheckBox chk_placeOnWalk;
        private System.Windows.Forms.PropertyGrid propertyGrid_item;
        private System.Windows.Forms.DataGridView dataGridView_furnis;
        private System.Windows.Forms.CheckBox chk_TopMost;
        private System.Windows.Forms.NumericUpDown nmr_minimumycoord;
        private System.Windows.Forms.Label lbl_minimum_coords;
        private System.Windows.Forms.NumericUpDown nmr_minimumxcoord;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox grp_form;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn classname;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn furniline;
        private System.Windows.Forms.DataGridViewTextBoxColumn revision;
        private System.Windows.Forms.DataGridViewTextBoxColumn defaultdir;
        private System.Windows.Forms.DataGridViewTextBoxColumn xdim;
        private System.Windows.Forms.DataGridViewTextBoxColumn ydim;
        private System.Windows.Forms.DataGridViewTextBoxColumn offerid;
        private System.Windows.Forms.DataGridViewTextBoxColumn buyout;
        private System.Windows.Forms.DataGridViewTextBoxColumn bc;
        private System.Windows.Forms.WebBrowser webBrowser_furni_preview;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_Placement;
        private System.Windows.Forms.TabPage tabPage_Preview;
        private System.Windows.Forms.LinkLabel linkLabel_Source;
    }
}

